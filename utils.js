import path from 'path'
import readline from 'readline'
import fs from 'fs-extra'
import { languagesDefined, langMatchers } from './file-extensions'

const walk = async (p, envObj, filesSet) => {
	try {
		const files = await fs.readdir(p)
		let len = files.length
		if (!len) return
		for (const file of files) {
			const dirFile = path.join(p, file)
			if (filesSet.has(dirFile)) len--
			try {
				const stat = await fs.stat(dirFile)
				if (stat.isFile()) {
					if (envObj.ignorePaths && envObj.ignorePaths(dirFile)) {
						envObj.logInPlace('Ignoring: ')(dirFile)
					} else {
						envObj.logInPlace('Checking: ')(dirFile)
						filesSet.add(dirFile)
					}
					len--
					if (!len) return
				} else if (stat.isDirectory()) {
					try {
						if (envObj.ignorePaths && envObj.ignorePaths(dirFile)) {
							envObj.logInPlace('Ignoring: ')(dirFile)
						} else {
							envObj.logInPlace('Checking: ')(dirFile)
							await walk(dirFile, envObj, filesSet)
							// filesSet.add(dirFile)
						}
						len--
						if (!len) return
					} catch (e) {
						envObj.logger.error('Subdir inaccessible:', e)
					}
				}
			} catch (e) {
				envObj.logger.error('Cannot stat file:', e)
			}
		}
	} catch (e) {
		envObj.logger.error('Directory walk failed:', e)
	}
}

const langResolver = langs => (l, type, nesting) => {
	let lnum = 0
	let resLang = type
	while (langs[lnum]) {
		const test = langMatchers[langs[lnum]].start(l)
		if (test) resLang = langs[lnum]
		lnum++
	}

	return resLang !== type ? resLang : null
}
const countSloc = async (file, envObj) => {
	return new Promise((resolve, reject) => {
		const extension = file
			.split('.')
			.pop()
			.toLowerCase()
		let isLangMulti = false
		let allComments = getCommentChars(extension)
		let type = extension
		let comments = null
		let langNesting = 0
		let resolveLang = null
		if (typeof allComments.line === 'undefined') {
			isLangMulti = true
			const types = Object.keys(allComments)
			type = types[0]
			resolveLang = langResolver(types)
			comments = allComments[type].comments
		} else {
			comments = allComments
		}
		const makeEntry = envObj.counts.entry(file, extension, isLangMulti)
		let sloc = 0
		let numComments = 0
		let blankLines = 0
		let loc = 0
		let inMultiline = false
		let inSlocSkip = false
		const rl = readline.createInterface({
			input: fs.createReadStream(file)
		})
		rl.on('close', x => {
			envObj.remaining--
			resolve(true)
		})
		rl.on('error', x => {
			envObj.remaining--
			reject(false)
		})
		rl.on('line', l => {
			if (isLangMulti) {
				const lang = resolveLang(l, type)
				if (lang) {
					type = lang
					comments = allComments[lang].comments
				}
			}
			const line = l.trim()
			makeEntry('lines', type)

			if (line.length === 0) {
				makeEntry('blanks', type)
				return
			}
			makeEntry('loc', type)
			// Check if a multi-line comment (comment block) starts
			if (comments.multi && line.indexOf(comments.multi.start) === 0) {
				const commentEnd = line.indexOf(comments.multi.end)
				makeEntry('comments', type)

				if (line.includes(`[sloccount:off]`)) {
					inSlocSkip = true
				}
				if (line.includes(`[sloccount:on]`)) {
					inSlocSkip = false
				}
				// Check if the multi-line comment ends at the same line
				if (commentEnd !== -1 && commentEnd !== 0) {
					return
				}

				inMultiline = !inMultiline
				return
			}

			if (inMultiline && line.indexOf(comments.multi.end) !== -1) {
				makeEntry('comments', type)
				inMultiline = false
				return
			}

			if (inMultiline) {
				makeEntry('comments', type)
				return
			}

			if (comments.line && line.indexOf(comments.line) === 0) {
				if (line.includes(`[countsloch:off]`)) {
					inSlocSkip = true
				}
				if (line.includes(`[countsloch:on]`)) {
					inSlocSkip = false
				}
				makeEntry('comments', type)
				return
			}
			if (inSlocSkip) {
				makeEntry('skipped', type)
				return
			}
			makeEntry('sloc', type)
		})
	})
}

const fileAllowed = (file, extensions) => {
	const extension = file
		.split('.')
		.pop()
		.toLowerCase()
	return extensions.includes(extension)
}

const filterFiles = (files, extensions) => {
	return files.filter(file => {
		return fileAllowed(file, extensions)
	})
}

function getCommentChars(extension) {
	const ext = languagesDefined.find(x => x.lang === extension)

	if (ext) {
		let res = {}
		if (Array.isArray(ext.comments)) {
			for (const c of ext.comments) {
				const multiExt = languagesDefined.find(x => x.lang === c)
				if (multiExt) res[c] = multiExt
			}
			return res
		} else return ext.comments
	} else {
		return {
			line: '//',
			multi: { start: '/*', end: '*/' }
		}
	}
}
export class GlobalCount {
	constructor(cf, model, mode) {
		this.cocomoFactors = cf
		this.cocomoModel = model
		this.cocomoMode = mode
		this.pattern = {
			loc: 0,
			sloc: 0,
			comments: 0,
			blanks: 0,
			lines: 0,
			skipped: 0
		}
		this.root = ''
		this.reset()
	}
	reset() {
		this.totals = { files: 0, ...this.pattern }
		this.files = {}
		this.tree = {}
		this.allLangs = {}
		this.langs = {}
		this.multi = {}
		this.cocomoLangs = {}
	}
	entry(filename, language, multi) {
		const rootRegex = RegExp(`^${this.root}`)
		const fileWithoutRoot = filename.replace(rootRegex, '')
		const splitFile = fileWithoutRoot.split(path.sep)
		let currObj = this.tree
		for (var i = 1; i < splitFile.length; i++) {
			if (typeof currObj[splitFile[i]] === 'undefined') {
				currObj[splitFile[i]] =
					i === splitFile.length - 1 ? { filename: fileWithoutRoot, lang: language, multi, ...this.pattern } : {}
			}
			currObj = currObj[splitFile[i]]
		}
		const treeObj = currObj
		const flatFilesId = `${String(Math.floor(Math.random() * 900000000000)).padEnd(12, '0')}|||${fileWithoutRoot}`
		const flatFilesObj = (this.files[flatFilesId] = {
			fileWithoutRoot,
			...this.pattern
		})
		const commonLangObj = { files: 0, ...this.pattern }
		if (typeof this[multi ? 'multi' : 'langs'][language] === 'undefined')
			this[multi ? 'multi' : 'langs'][language] = commonLangObj
		const langObj = this[multi ? 'multi' : 'langs'][language]
		const incrBundle = [flatFilesObj, treeObj, langObj]
		this.totals.files++
		langObj.files++
		return (what, lang = language) => {
			if (what !== 'files') {
				if (typeof this.allLangs[lang] === 'undefined') this.allLangs[lang] = { ...this.pattern }
				this.allLangs[lang][what]++
				incrBundle.map(x => x[what]++)
				this.totals[what]++
			}
		}
	}
	showAllCounts() {
		const coc = source => {
			const kloc = source / 1000
			const cVer = this.cocomoFactors[this.cocomoModel][this.cocomoMode]
			const effort = cVer.a * Math.pow(kloc, cVer.b)
			const duration = cVer.c * Math.pow(effort, cVer.d)
			const staffing = effort / duration
			return { effort, duration, staffing }
		}
		this.cocomo = coc(this.totals.sloc)
		return {
			metrics: { timestamp: Date.now() },
			totals: this.totals,
			cocomo: this.cocomo,
			files: this.files,
			tree: this.tree,
			allLangs: this.allLangs,
			cocomoLangs: this.cocomoLangs,
			langs: this.langs,
			multi: this.multi
		}
	}
}

module.exports = {
	walk,
	countSloc,
	filterFiles,
	fileAllowed,
	GlobalCount
}
