const fs = require('fs-extra')
const utils = require('./utils')
import pathUtils from 'path'
import extensionsMap from './file-extensions'
import { GlobalCount } from './utils'
const allowedExtensions = extensionsMap.map(x => x.lang)
import picomatch from 'picomatch'

const walkCountSingle = async (path, envObj, filesSet) => {
	path = pathUtils.resolve(path)
	if (!(await fs.exists)(path)) {
		return
	}
	if ((await fs.stat(path)).isFile()) {
		if (utils.fileAllowed(path, envObj.extensions) && !filesSet.has(path)) {
			if (envObj.ignorePaths && envObj.ignorePaths(path)) return
			envObj.logInPlace('Checking: ')(path)
			filesSet.add(path)
			return
		}
	} else if ((await fs.stat(path)).isDirectory()) {
		try {
			return utils.walk(path, envObj, filesSet)
		} catch (e) {
			envObj.logger.error('Error', e)
		}
	}
}

const minLogLevels = ['error', 'warn', 'info', 'debug']

const logInPlace = () => {}
export class CountSlocH {
	constructor(options) {
		this.env = {
			logLevel: 3,
			logMargin: 10,
			logPadUp: 1,
			ignore: ['node_modules/'],
			cocomoModel: 'basic',
			cocomoMode: 'organic',
			cocomo: {
				basic: {
					organic: {
						a: 2.4,
						b: 1.05,
						c: 2.5,
						d: 0.38
					},
					semiDetached: {
						a: 3,
						b: 1.12,
						c: 2.5,
						d: 0.35
					},
					embedded: {
						a: 3.6,
						b: 1.2,
						c: 2.5,
						d: 0.32
					}
				}
			}
		}
		this.update(options)
	}

	update(options) {
		if (typeof options !== 'object' || !options) {
			throw new Error('Options must be an object and must be provided')
		}
		if (typeof options.path !== 'string' && !Array.isArray(options.path)) {
			throw new Error('Providing path in options is obligatory')
		}
		if (options.ignoreDefault && !options.extensions) {
			throw new Error('Alternative extensions must be supplied when ignoreDefault is set')
		}
		this.env = Object.assign({}, this.env, options)
		if (options.logger) {
			const gOPN = Object.getOwnPropertyNames(options.logger)
			if (gOPN.filter(x => minLogLevels.includes(x)).length !== 4) this.env.logger = console
		}
		this.env.counts = new GlobalCount(this.env.cocomo, this.env.cocomoModel, this.env.cocomoMode)
		this.env.logInPlace = (prefix, suffix = '', margin = this.env.logMargin) => {
			// TODO: Multiline
			const availableSpace = process.stdout.columns - (margin + prefix.length + suffix.length)
			const calcExtent = text => {
				const sliceExtent = text.length >= availableSpace - 3 ? text.length - (availableSpace - 3) : 0
				const sliceFd = `${sliceExtent != 0 ? '...' : ''}${text.slice(sliceExtent)}`
				this.env.logger.info(`${prefix}${sliceFd}${suffix}`.padEnd(process.stdout.columns - margin, ' '))
			}
			return this.env.logLevel > 1
				? (text = '') => {
						for (var i = 0; i < this.env.logPadUp; i++) {
							process.stdout.write('\x1B[1A')
						}
						calcExtent(text)
				  }
				: () => {}
		}
		let extensions = allowedExtensions
		this.env.ignorePaths =
			Array.isArray(options.ignore) || typeof options.ignore === 'string'
				? picomatch(options.ignore, { contains: true })
				: null
		if (options.extensions) {
			// Don't use the default extensions if ignoreDefault is true
			const ext = options.ignoreDefault ? [] : allowedExtensions
			extensions = [...ext, ...options.extensions]
		}
		this.env.extensions = extensions
		this.env.counts.root = options.root || ''
		this.env.remaining = 0
	}

	async scan() {
		const files = await this.walkAndCount()
		const lenFiles = files.length
		if (lenFiles === 0) {
			this.env.logInPlace(`No files found, aborting`)
			return
		}
		const processedFiles = await this.processFiles(files)
		const retData = this.env.counts.showAllCounts()
		this.env.logInPlace('Done processing ', ' files')(processedFiles.toString())
		return retData
	}

	getExtensions() {
		return this.env.extensions
	}

	async walkAndCount() {
		let filesSet = new Set()
		if (typeof this.env.path === 'string') {
			await walkCountSingle(path, this.env, filesSet)
		}
		if (Array.isArray(this.env.path)) {
			for (const path of this.env.path) {
				if (typeof path === 'string') {
					await walkCountSingle(path, this.env, filesSet)
				}
			}
		}
		return [...filesSet]
	}
	async processFiles(files) {
		try {
			let promises = []

			let filteredPaths = utils.filterFiles(files, this.env.extensions || [])
			this.env.remaining = filteredPaths.length
			this.env.logInPlace(`Found `, ` matching files - processing...`)(this.env.remaining.toString())

			filteredPaths.forEach(fpath => {
				promises.push(utils.countSloc(fpath, this.env))
			})
			let intvl = setInterval(x => {
				this.env.logInPlace(`Found `, ` matching files - processing...`)(this.env.remaining.toString())
			}, 100)
			await Promise.all(promises)
			this.env.logInPlace(`Found `, ` matching files - processing...`)(this.env.remaining.toString())
			clearInterval(intvl)
			return filteredPaths.length
		} catch (e) {
			this.env.logger.error('Scanning files failed', e)
			return 0
		}
	}
}
export const countSlocH = options => new CountSlocH(options)
export default countSlocH
